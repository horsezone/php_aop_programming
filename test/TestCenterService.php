<?php
namespace aop\test;

use \aop\AOPConstructor;
use \aop\test\TestOneClass;
use \aop\test\TestTwoClass;
use \aop\test\TestThreeClass;
use \aop\test\TestFourClass;

class TestCenterService {

    use \aop\test\TestBaseTrait;

    public function testSameObjectOneMethodOnce() {
        $testOneClass = new TestOneClass;
        $aopConstructor = new AOPConstructor;

        $startA = microtime();
        $testOneClass->commonOne();
        $endA = microtime();
        $this->monitor($startA, $endA, 'common_same_object_one_method_once');

        $startB = microtime();
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $endB = microtime();
        $this->monitor($startB, $endB, 'aop_same_object_one_method_once');
    }

    public function testSameObjectOneMethodTwice() {
        $testOneClass = new TestOneClass;
        $aopConstructor = new AOPConstructor;

        $startA = microtime();
        $testOneClass->commonOne();
        $testOneClass->commonOne();
        $endA = microtime();
        $this->monitor($startA, $endA, 'common_same_object_one_method_twice');

        $startB = microtime();
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $endB = microtime();
        $this->monitor($startB, $endB, 'aop_same_object_one_method_twice');
    }

    public function testSameObjectOneMethodThird() {
        $testOneClass = new TestOneClass;
        $aopConstructor = new AOPConstructor;

        $startA = microtime();
        $testOneClass->commonOne();
        $testOneClass->commonOne();
        $testOneClass->commonOne();
        $endA = microtime();
        $this->monitor($startA, $endA, 'common_same_object_one_method_third');

        $startB = microtime();
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $endB = microtime();
        $this->monitor($startB, $endB, 'aop_same_object_one_method_third');
    }

    public function testSameObjectOneMethodFourth() {
        $testOneClass = new TestOneClass;
        $aopConstructor = new AOPConstructor;

        $startA = microtime();
        $testOneClass->commonOne();
        $testOneClass->commonOne();
        $testOneClass->commonOne();
        $testOneClass->commonOne();
        $endA = microtime();
        $this->monitor($startA, $endA, 'common_same_object_one_method_fourth');

        $startB = microtime();
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $endB = microtime();
        $this->monitor($startB, $endB, 'aop_same_object_one_method_fourth');
    }

    public function testSameObjectTwoDifferentMethod() {
        $testOneClass = new TestOneClass;
        $aopConstructor = new AOPConstructor;

        $startA = microtime();
        $testOneClass->commonOne();
        $testOneClass->commonTwo();
        $endA = microtime();
        $this->monitor($startA, $endA, 'common_same_object_two_different_method');

        $startB = microtime();
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopTwo');
        $endB = microtime();
        $this->monitor($startB, $endB, 'aop_same_object_two_different_method');
    }

    public function testSameObjectThreeDifferentMethod() {
        $testOneClass = new TestOneClass;
        $aopConstructor = new AOPConstructor;

        $startA = microtime();
        $testOneClass->commonOne();
        $testOneClass->commonTwo();
        $testOneClass->commonThree();
        $endA = microtime();
        $this->monitor($startA, $endA, 'common_same_object_three_different_method');

        $startB = microtime();
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopTwo');
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopThree');
        $endB = microtime();
        $this->monitor($startB, $endB, 'aop_same_object_three_different_method');
    }

    public function testSameObjectFourDifferentMethod() {
        $testOneClass = new TestOneClass;
        $aopConstructor = new AOPConstructor;

        $startA = microtime();
        $testOneClass->commonOne();
        $testOneClass->commonTwo();
        $testOneClass->commonThree();
        $testOneClass->commonFour();
        $endA = microtime();
        $this->monitor($startA, $endA, 'common_same_object_four_different_method');

        $startB = microtime();
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopTwo');
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopThree');
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopFour');
        $endB = microtime();
        $this->monitor($startB, $endB, 'aop_same_object_four_different_method');
    }

    public function testTwoObjectOneMethod() {
        $testOneClass = new TestOneClass;
        $testTwoClass = new TestTwoClass;
        $aopConstructor = new AOPConstructor;

        $startA = microtime();
        $testOneClass->commonOne();
        $testTwoClass->commonOne();
        $endA = microtime();
        $this->monitor($startA, $endA, 'common_two_object_one_method');

        $startB = microtime();
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $aopConstructor->execute('test', TestTwoClass::class, $testTwoClass, 'aopOne');
        $endB = microtime();
        $this->monitor($startB, $endB, 'aop_two_object_one_method');
    }

    public function testThreeObjectOneMethod() {
        $testOneClass = new TestOneClass;
        $testTwoClass = new TestTwoClass;
        $testThreeClass = new TestThreeClass;
        $aopConstructor = new AOPConstructor;

        $startA = microtime();
        $testOneClass->commonOne();
        $testTwoClass->commonOne();
        $testThreeClass->commonOne();
        $endA = microtime();
        $this->monitor($startA, $endA, 'common_three_object_one_method');

        $startB = microtime();
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $aopConstructor->execute('test', TestTwoClass::class, $testTwoClass, 'aopOne');
        $aopConstructor->execute('test', TestThreeClass::class, $testThreeClass, 'aopOne');
        $endB = microtime();
        $this->monitor($startB, $endB, 'aop_three_object_one_method');
    }

    public function testFourObjectOneMethod() {
        $testOneClass = new TestOneClass;
        $testTwoClass = new TestTwoClass;
        $testThreeClass = new TestThreeClass;
        $testFourClass = new TestFourClass;
        $aopConstructor = new AOPConstructor;

        $startA = microtime();
        $testOneClass->commonOne();
        $testTwoClass->commonOne();
        $testThreeClass->commonOne();
        $testFourClass->commonOne();
        $endA = microtime();
        $this->monitor($startA, $endA, 'common_four_object_one_method');

        $startB = microtime();
        $aopConstructor->execute('test', TestOneClass::class, $testOneClass, 'aopOne');
        $aopConstructor->execute('test', TestTwoClass::class, $testTwoClass, 'aopOne');
        $aopConstructor->execute('test', TestThreeClass::class, $testThreeClass, 'aopOne');
        $aopConstructor->execute('test', TestFourClass::class, $testFourClass, 'aopOne');
        $endB = microtime();
        $this->monitor($startB, $endB, 'aop_four_object_one_method');
    }
}