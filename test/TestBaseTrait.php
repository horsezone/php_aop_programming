<?php
//仅仅用于测试微框架的耗时

namespace aop\test;

Trait TestBaseTrait {

    protected function monitor($start, $end, $type) {
        list($startMicro, $startSec) = explode(' ', $start);
        list($endMicro, $endSec) = explode(' ', $end);
        $startSec = intval($startSec);
        $endSec = intval($endSec);
        $startMicro = intval(substr($startMicro, 2, 6));
        $endMicro = intval(substr($endMicro, 2, 6));
        $consumeTime = ($endSec - $startSec) * 1000000 + $endMicro - $startMicro;
        @file_put_contents(TMP_DIR . $type, $consumeTime . "\n", FILE_APPEND);
    }
}