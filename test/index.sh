#!/bin/sh
# version 1.0
# 框架性能测试脚本

function aoptest(){
    local i=0
    while [ "$i" != "10" ];
    do
        php ../index.php testSameObjectOneMethodOnce
        php ../index.php testSameObjectOneMethodTwice
        php ../index.php testSameObjectOneMethodThird
        php ../index.php testSameObjectOneMethodFourth
        php ../index.php testSameObjectTwoDifferentMethod
        php ../index.php testSameObjectThreeDifferentMethod
        php ../index.php testSameObjectFourDifferentMethod
        php ../index.php testTwoObjectOneMethod
        php ../index.php testThreeObjectOneMethod
        php ../index.php testFourObjectOneMethod
        i=$(($i+1))
        printf "$i\n"
    done
}

j=0
while [ "$j" != "2" ];
do
    j=$(($j+1))
    aoptest #&
done

php consuming.php
exit