<?php
/**
 * 个人项目的入口文件
 */

class AOP {
	public function _bind($abstract, $concret, $method, array $params = array()) {
		$aopConstructor = new \aop\AOPConstructor;
		return $aopConstructor->execute('example', $abstract, $concret, $method, $params);
	}
}

return new AOP;