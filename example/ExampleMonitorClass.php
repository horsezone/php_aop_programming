<?php
namespace aop\example;

class ExampleMonitorClass {

    use \aop\config\MonitorOperationTrait,
        \aop\config\ReplaceReturnDataTrait;

    public function monitorAny() {
        $params = func_get_arg(0);
        @file_put_contents(TMP_DIR . 'example_info', json_encode($params) . "\n", FILE_APPEND);
    }

    public function monitorParam() {
        $params = func_get_arg(0);
        @file_put_contents(TMP_DIR . 'example_info', json_encode($this->_monitorExampleCenter($params)) . "\n", FILE_APPEND);
    }

    public function replaceParam() {
        $params = func_get_arg(0);
        $obj = new \stdClass();
        $obj->success = FALSE;
        $obj->message = 'replace';
        $obj->data = __METHOD__ . ' test';
        return $this->_outputReplaceData(TRUE, $obj);
    }
}