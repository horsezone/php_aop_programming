<?php
namespace aop\config;

trait ReplaceReturnDataTrait {

    /**
     * 用于在使用 replace 关键词时，返回正确的格式
     * replace 关键词表示在执行完 replace 内的逻辑后，是否执行原有的函数逻辑以及 interpret 关键词内的逻辑
     * @param  [boolean] $success [TRUE 表示 不执行 原有的函数逻辑以及 interpret 关键词内的逻辑；FALSE 表示在 继续执行 原有的函数逻辑以及 interpret 关键词内的逻辑]
     * @param  [void]    $data    [当上方为TRUE时，会将该值作为原有函数逻辑的返回值]
     * @return [array]
     */
    final protected function _outputReplaceData($success, $data) {
        return ['success' => $success, 'data' => $data];
    }
}