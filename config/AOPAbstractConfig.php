<?php
namespace aop\config;

trait AOPAbstractConfig {

    private $_abstract_config_map = [
        'test' => [
            'MonitorService' => \aop\test\MonitorService::class
        ],
        'example' => [
            'ExampleMonitorClass' => \aop\example\ExampleMonitorClass::class
        ]
    ];
}