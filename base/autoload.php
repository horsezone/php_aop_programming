<?php
function _register($abstract) {
	if (!$abstract) {
		return;
	}
	$baseNamespaceLen = strlen(BASE_NAMESPACE);
	if (substr($abstract, 0, $baseNamespaceLen) !== BASE_NAMESPACE) {
		return;
	}

	$filepath = APP_DIR . str_replace('\\', PATH_SEPERATOR, substr($abstract, $baseNamespaceLen)) . FILE_EXT;
	file_exists($filepath) && require_once $filepath;
}

spl_autoload_register('_register');